;;; packages.el --- erc Layer packages File for Spacemacs
;;
;; Copyright (c) 2012-2014 Sylvain Benner
;; Copyright (c) 2014-2015 Sylvain Benner & Contributors
;;
;; Author: Sylvain Benner <sylvain.benner@gmail.com>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

(defvar erc-packages '(
                       erc
                       erc-hl-nicks
                       erc-image
                       erc-yt
                       )
  "List of all packages to install and/or initialize. Built-in packages
which require an initialization must be listed explicitly in the list.")

(when (system-is-mac)
  (push 'erc-terminal-notifier erc-packages))

(defvar erc-excluded-packages '()
  "List of packages to exclude.")

(defvar erc-prompt-default '(lambda () (concat "[" (buffer-name) "]"))
  "Default input prompt.")

(defun erc/init-erc ()
  "Initialize ERC"
  (use-package erc
    :defer t
    :init
    (evil-leader/set-key
      "aie" 'erc
      "aiE" 'erc-tls)
    (setq erc-server-coding-system '(utf-8 . utf-8))
    (defun no-linum (&rest ignore)
      (when (or 'linum-mode global-linum-mode)
        (linum-mode 0)))
    (add-to-hooks 'no-linum '(erc-hook
                              erc-mode-hook
                              erc-insert-pre-hook))

    :config
    (progn
      (use-package erc-autoaway
        :config
        (setq erc-auto-discard-away t
              erc-autoaway-idle-seconds 600
              erc-autoaway-use-emacs-idle t))
      (defun erc-list-command ()
        "execute the list command"
        (interactive)
        (insert "/list")
        (erc-send-current-line))

      (setq erc-kill-buffer-on-part t
            erc-kill-queries-on-quit t
            erc-kill-server-buffer-on-quit t)
      (add-hook 'erc-connect-pre-hook (lambda (x) (erc-update-modules)))

      (erc-track-mode t)
      (setq erc-track-exclude-types '("JOIN" "NICK" "PART" "QUIT" "MODE")
            erc-server-coding-system '(utf-8 . utf-8))
      (setq erc-prompt erc-prompt-default)
      (require 'notifications)
      (defun erc-global-notify (match-type nick message)
        "Notify when a message is recieved."
        (notifications-notify
         :title nick
         :body message
         :app-icon "/home/elken/.emacs.d/assets/spacemacs.svg"
         :urgency 'low))

      (add-hook 'erc-text-matched-hook 'erc-global-notify)
      ;; keybindings
      (evil-leader/set-key-for-mode 'erc-mode
        "mb" 'erc-iswitchb
        "md" 'erc-input-action
        "mj" 'erc-join-channel
        "mn" 'erc-channel-names
        "ml" 'erc-list-command
        "mp" 'erc-part-from-channel
        "mq" 'erc-quit-server))))

(defun erc/init-erc-hl-nicks ()
  (use-package erc-hl-nicks
    :defer t))

(defun erc/init-erc-image ()
  (use-package erc-image
    :init (eval-after-load 'erc '(add-to-list 'erc-modules 'image))))

(defun erc/init-erc-yt ()
  (use-package erc-yt
    :init (eval-after-load 'erc '(add-to-list 'erc-modules 'youtube))))

(defun erc/init-erc-terminal-notifier ())
